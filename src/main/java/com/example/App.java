package com.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;

import static org.openqa.selenium.By.id;

public class App {

    private static String username;
    private static String password;
    private static String changeSetNames;
    private static String instanceNames;
    private static String driverLocation;

    public static void main(String[] args) throws IOException {

        //Get data from properties
        getProperties();

        System.setProperty("webdriver.chrome.driver", driverLocation);

        WebDriver driver = new ChromeDriver();

        driver.get("https://test.salesforce.com");

        driver.manage().window().maximize();

        //Login
        driver.findElement(id("username")).sendKeys(username);
        driver.findElement(id("password")).sendKeys(password);
        driver.findElement(id("Login")).click();

        //Wait until logo appear, maybe can be removed
        WebDriverWait wait = new WebDriverWait(driver, 20);
        By salesforceLogo = By.id("phHeaderLogoImage");
        wait.until(ExpectedConditions.presenceOfElementLocated(salesforceLogo));

        //Get host
        String host;
        try {
            URL currentUrl = new URL(driver.getCurrentUrl());
            host = currentUrl.getHost();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }

        //Open outbound changesets page
        driver.get(String.format("https://%s/changemgmt/listOutboundChangeSet.apexp", host));

        Iterator<String> changeSetIterator = Arrays.stream(changeSetNames.split(",")).iterator();
        String changeSetName;

        while (changeSetIterator.hasNext()) {

            changeSetName = changeSetIterator.next();

            //Get link to changeset
            WebElement linkToChangesetEl = driver.findElement(By.partialLinkText(changeSetName));
            String linkToChangeset = linkToChangesetEl.getAttribute("href");

            //Open page with changeset
            driver.get(linkToChangeset);

            Iterator<String> instanceNameIterator = Arrays.stream(instanceNames.split(",")).iterator();
            String instanceName;

            while (instanceNameIterator.hasNext()) {

                instanceName = instanceNameIterator.next();

                while (true) {

                    //Click on upload button
                    driver.findElement(By.xpath("//input[@type='submit' and @value='Upload']")).click();

                    //Select radio button with instance to which need to deploy
                    driver.findElement(By.xpath(String.format("//td[text()='%s']//preceding::td[1]/child::span//child::input", instanceName))).click();

                    //Click on upload button
                    driver.findElement(By.xpath("//input[@type='submit' and @value='Upload']")).click();

                    //Wait until upload is finished
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h2[text()='Change Set Detail']")));

                    //Check if changeset successfully uploaded
                    if (driver.findElement(By.xpath("//img[@title='info' or @title='confirm']")) != null) {
                        System.out.println(String.format("%s successfully uploaded", changeSetName));
                        break;
                    } else {
                        System.out.println(String.format("%s upload failed", changeSetName));
                    }

                }

            }

            //Open page with changesets
            driver.get(String.format("https://%s/changemgmt/listOutboundChangeSet.apexp", host));

        }

        driver.quit();
    }

    private static void getProperties() throws IOException {
        Properties properties = new Properties();
        FileInputStream file = new FileInputStream("./salesforce-changeset-uploader.properties");
        properties.load(file);
        file.close();
        username = properties.getProperty("username");
        password = properties.getProperty("password");
        changeSetNames = properties.getProperty("changesets");
        instanceNames = properties.getProperty("instances");
        driverLocation = properties.getProperty("driverLocation");
    }
}
